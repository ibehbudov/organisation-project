<?php

declare(strict_types=1);

use App\Organisation;
use App\User;
use Illuminate\Database\Seeder;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 200)
            ->create()->each(static function (User $user) {
                factory(Organisation::class)->create([
                    'owner_user_id' => $user->id,
                ]);
            });
    }
}
