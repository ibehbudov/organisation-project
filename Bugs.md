# Create an endpoint to create an organisation. 
   A few files will need to be completed for this to work.
   Criteria for completion:
   Request must be validated. - Валидация в реквесте есть и сам реквест, но она не отрабатывает https://prnt.sc/12mgnzh , 
   ошибка 500 . Ошибки вернуть придерживаясь общей структуры ответа https://prnt.sc/12mgz1j https://prnt.sc/12mh0nv
- A user must be logged in to complete the request. - Done.
- Organisations should be created with trial period of 30 days. - Done.
- An email should be triggered upon organisation creation that send the logged in user a confirmation. (just a plaintext email with the details is perfect for this test.) - Done
- The JSON response should include the user inside of the organisation. 
  Half of this has been completed, you will need to create a transformer for the User and include the data that you believe to be relevant. - Done. Также добавлена пагинация

# Fix the code that outputs a collection of organisations
- Use the transformer to return the data (hint: transformCollection) - Done.
- The endpoint should be able to take a http query param of filter, which can be subbed, trial or all. If not included, all is the default case. - Done
- Abstract the controller logic into the service - Done.
- Code must adhere to PSR12 standards. - Done. Есть мелкие нюансы https://prnt.sc/12mhp88
- All datetimes returned must be in unix timestamps. - Done.
- Code should include docblocks. - Внимательно перепроверить все методы, не везде актуальные аннотации или их нет. https://prnt.sc/12mhcnq https://prnt.sc/12mhe5j
#  Итого:
- поправить валидацию и ответы, придерживаться одной структуры ответа, опираясь на ApiController. https://prnt.sc/12mgimf - В ответе не нравятся эти ключи в json, просто по такому типу - organisations: [{}, {}, {}, ...]
- https://prnt.sc/12meo6q - Не отработал seed при migrate:fresh --seed. Поправить сиды, чтоб запускались без ошибок
- Проверить и поправить аннотации
- Пересмотреть классы созданные и лишнее поудалять - https://prnt.sc/12mhgn0 https://prnt.sc/12mhibr
- И можно пересмотреть типизацию в методах, так как используется declare(strict_types=1); https://prnt.sc/12mik9i В некоторых местах она есть, в некоторых нет
   Понравилось акуратность кода.
