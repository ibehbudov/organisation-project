<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\OrganisationService;
use App\Validators\OrganisationValidator;
use Illuminate\Http\JsonResponse;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{
    /**
     * Create new organisation
     *
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    public function store(OrganisationService $service): JsonResponse
    {
        $this
            ->setValidator(new OrganisationValidator())
            ->setFormData($this->request->all());

        if(! $this->runValidation()) {
            return $this
                ->appendValidationError()
                ->respond();
        }

        $organisation = $service->createOrganisation($this->request->all());

        return $this
            ->transformItem('organisation', $organisation)
            ->respond();
    }

    /**
     * List all organisations by filtering subscription
     *
     * @param OrganisationService $service
     * @return JsonResponse
     */
    public function listAll(OrganisationService $service): JsonResponse
    {
        $this
            ->setValidator(new OrganisationValidator())
            ->setFormData($this->request->all());

        if(! $this->runValidation()) {
            return $this
                ->appendValidationError()
                ->respond();
        }

        $organisations = $service->filter($this->request->get('filter'));

        return $this
            ->transformCollection('organisations', $organisations)
            ->respond();
    }
}
