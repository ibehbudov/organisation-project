<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Serializers\DefaultSerializer;
use App\Validators\CustomValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    use CustomValidator;

    /**
     * Client request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response status code
     *
     * @var int
     */
    protected $statusCode;

    /**
     * Response body
     *
     * @var array
     */
    protected $body;

    /**
     * Model transformer
     *
     * @var
     */
    protected $transformer;

    /**
     * AbstractApiController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return JsonResponse
     */
    public function respond(): JsonResponse
    {
        return \response()->json($this->body, $this->getStatusCode());
    }

    /**
     * Returns response status code
     *
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode ?? Response::HTTP_OK;
    }

    /**
     * Set response status code
     *
     * @param int $statusCode
     *
     * @return ApiController
     */
    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Append data to response body
     *
     * @param string $key
     * @param        $data
     * @param bool   $mainObject
     * @return ApiController
     */
    public function appendBody(string $key, $data, $mainObject = false): self
    {
        if ($mainObject) {
            $this->body[$key] = $data;
            return $this;
        }
        $this->body['data'][$key] = $data;

        return $this;
    }

    /**
     *
     * Append error message to response body
     *
     * @param string $message
     *
     * @return ApiController
     */
    public function appendError(string $message): self
    {
        return $this->appendBody('error', $message)
                    ->setStatusCode(Response::HTTP_FORBIDDEN);
    }

    /**
     * Transform response body for single data
     *
     * @param string $key
     * @param        $data
     * @return ApiController
     */
    public function transformItem(string $key, $data): self
    {
        $item = fractal()
            ->item($data, $this->getTransformer())
            ->serializeWith($this->getSerializer())
            ->toArray();

        $this->appendBody($key, $item);

        return $this;
    }

    /**
     * Transform response body for collection data
     *
     * @param string $key
     * @param        $data
     * @return ApiController
     */
    public function transformCollection(string $key, $data): self
    {
        $data->appends(
            array_diff_key(\request()->all(),
            array_flip(['page']))
        );

        $collection = fractal()
            ->collection($data, $this->getTransformer())
            ->serializeWith($this->getSerializer())
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->toArray();

        $meta = isset($collection['meta']) ? $collection['meta'] : [];

        unset($collection['meta']);

        $this->appendBody($key, $collection);
        $this->appendBody('meta', $meta);

        return $this;
    }

    /**
     * Get serializer
     *
     * @return DefaultSerializer
     */
    public function getSerializer(): DefaultSerializer
    {
        return new DefaultSerializer();
    }

    /**
     * Get default transformer for suitable model
     *
     * @return mixed
     */
    public function getDefaultTransformer()
    {
        $class = '\\App\\Transformers\\';

        $class = $class . ucfirst($this->request->get('_controller')) . 'Transformer';

        $class = new $class();

        return new $class();
    }

    /**
     * Set transformer
     *
     * @param $transformer
     *
     * @return ApiController
     */
    public function setTransformer($transformer): self
    {
        $this->transformer = $transformer;

        return $this;
    }

    /**
     * Get transformer
     *
     * @return mixed
     */
    public function getTransformer()
    {
        return $this->transformer ?? $this->getDefaultTransformer();
    }
}
