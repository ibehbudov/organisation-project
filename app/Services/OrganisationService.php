<?php

declare(strict_types=1);

namespace App\Services;

use App\Organisation;

/**
 * Class OrganisationService
 * @package App\Services
 */
class OrganisationService
{
    /**
     * @param array $attributes
     *
     * @return Organisation
     */
    public function createOrganisation(array $attributes): Organisation
    {
        $organisation = new Organisation($attributes);

        $organisation->save();

        return $organisation;
    }

    /**
     * List all organisations by filtering subscription
     *
     * @param string|null $queryFilter
     * @return mixed
     */
    public function filter(?string $queryFilter)
    {
        $organisation = new Organisation();

        switch ($queryFilter) {
            case 'subbed':
                $organisation = $organisation->subscribed(true);
                break;
            case 'trial':
                $organisation = $organisation->subscribed(false);
                break;
        }

        return $organisation->paginate(10);
    }


}
