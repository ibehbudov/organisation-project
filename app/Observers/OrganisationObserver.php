<?php

declare(strict_types=1);

namespace App\Observers;

use App\Organisation;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class OrganisationObserver
{
    /**
     * Handle the organisation "creating" event.
     *
     * @param Organisation $organisation
     * @return void
     */
    public function creating(Organisation $organisation)
    {
        if(! App::runningInConsole() ) {
            $organisation->setAttribute('owner_user_id', Auth::id());
            $organisation->setAttribute('trial_end', now()->addDays(30));
            $organisation->setAttribute('subscribed', false);
        }
    }
}
