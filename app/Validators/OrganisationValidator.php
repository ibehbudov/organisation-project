<?php

declare(strict_types=1);

namespace App\Validators;

class OrganisationValidator
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch (strtolower(request()->method())) {
            case 'post':
                return $this->rulesPost();
            case 'get':
            default:
                return $this->rulesGet();
        }
    }

    /**
     * Get rules of post method
     *
     * @return string[]
     */
    public function rulesPost(): array
    {
        return [
            'name'  =>  'required|min:3|max:255',
        ];
    }

    /**
     * Get rules of get method
     *
     * @return string[]
     */
    public function rulesGet(): array
    {
        return [
            'filter'  =>  'in:subbed,trial,all',
        ];
    }

    /**
     * Define error messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' =>  'Поле имена организация должен обязательно',
            'name.min'      =>  'Длина поле имена организация не должен меньше :min',
            'name.max'      =>  'Длина поле имена организация не должен больше :max',
            'filter.in'     =>  'Выбранный фильтр недействителен.'
        ];
    }
}
