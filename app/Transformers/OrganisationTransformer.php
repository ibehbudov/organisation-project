<?php

declare(strict_types=1);

namespace App\Transformers;

use App\Organisation;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * Class OrganisationTransformer
 * @package App\Transformers
 */
class OrganisationTransformer extends TransformerAbstract
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'owner'
    ];

    /**
     * @param Organisation $organisation
     *
     * @return array
     */
    public function transform(Organisation $organisation): array
    {
        return [
            'name'          =>  $organisation->name,
            'trial_end'     =>  optional($organisation->trial_end)->unix(),
            'subscribed'    =>  $organisation->subscribed
        ];
    }

    /**
     * @param Organisation $organisation
     *
     * @return Item
     */
    public function includeOwner(Organisation $organisation): Item
    {
        return $this->item($organisation->owner, new UserTransformer());
    }
}
