<?php

declare(strict_types=1);

namespace App\Mail;

use App\Organisation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrganisationCreatedMail extends Mailable implements ShouldQueue
{
    use Queueable;
    use SerializesModels;

    /**
     * Organisation model
     *
     * @var Organisation
     */
    public $organisation;

    /**
     * Create a new message instance.
     *
     * @param Organisation $organisation
     */
    public function __construct(Organisation $organisation)
    {
        $this->organisation = $organisation;

        $this
            ->to($organisation->owner->email)
            ->subject("Organisation {$organisation->name} is created")
            ->with('organisation', $organisation);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.organisation.created');
    }
}
