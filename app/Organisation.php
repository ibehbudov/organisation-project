<?php

declare(strict_types=1);

namespace App;

use App\Events\OrganisationCreated;
use App\Observers\OrganisationObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Organisation
 *
 * @property int         id
 * @property string      name
 * @property int         owner_user_id
 * @property Carbon      trial_end
 * @property bool        subscribed
 * @property Carbon      created_at
 * @property Carbon      updated_at
 * @property Carbon|null deleted_at
 *
 * @package App
 */
class Organisation extends Model
{
    use SoftDeletes;

    /**
     * Fillable attributes
     *
     * @var array
     */
    protected $fillable = ['name', 'owner_user_id'];

    /**
     * Date attributes
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'trial_end'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var string[]
     */
    protected $casts = [
        'subscribed'    =>  'boolean'
    ];

    /**
     * Dispatched events
     *
     * @var string[]
     */
    protected $dispatchesEvents = [
        'created'   =>  OrganisationCreated::class,
    ];

    /**
     * Model boot
     */
    protected static function boot()
    {
        parent::boot();

        Organisation::observe(OrganisationObserver::class);
    }

    /**
     * Returns owner of organisation
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_user_id');
    }

    /**
     * Scope a query to filtering for subscription.
     *
     * @param Builder $query
     * @param bool $isSubscribed
     * @return Builder
     */
    public function scopeSubscribed(Builder $query, bool $isSubscribed): Builder
    {
        return $query->where('subscribed', $isSubscribed);
    }
}
