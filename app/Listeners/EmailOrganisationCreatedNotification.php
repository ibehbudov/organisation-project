<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\OrganisationCreated;
use App\Mail\OrganisationCreatedMail;
use Illuminate\Support\Facades\Mail;

class EmailOrganisationCreatedNotification
{
    /**
     * Handle the event.
     *
     * @param  OrganisationCreated  $event
     * @return void
     */
    public function handle(OrganisationCreated $event)
    {
        Mail::send(
            new OrganisationCreatedMail($event->organisation)
        );
    }
}
